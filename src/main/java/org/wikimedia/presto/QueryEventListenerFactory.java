package org.wikimedia.presto;

import java.util.Map;

import com.facebook.presto.spi.eventlistener.EventListener;
import com.facebook.presto.spi.eventlistener.EventListenerFactory;

public class QueryEventListenerFactory implements EventListenerFactory {
    public String getName() {
        return "event-listener";
    }

    public EventListener create(Map<String, String> config) {
        return new QueryEventListener(config);
    }
}
