package org.wikimedia.presto;

import java.util.ArrayList;
import java.util.List;
import java.util.Collections;

import com.facebook.presto.spi.Plugin;
import com.facebook.presto.spi.eventlistener.EventListenerFactory;

public class QueryEventListenerPlugin implements Plugin {

    public Iterable<EventListenerFactory> getEventListenerFactories() {
        EventListenerFactory listenerFactory = new QueryEventListenerFactory();
        List<EventListenerFactory> listenerFactoryList = new ArrayList<EventListenerFactory>();
        listenerFactoryList.add(listenerFactory);
        List<EventListenerFactory> immutableList = Collections.unmodifiableList(listenerFactoryList);

        return immutableList;

    }
}
